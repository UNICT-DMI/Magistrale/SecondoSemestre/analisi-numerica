% Interpolazione della funzione di Runge 
%
clear all;
a = -4.5; % estremi dell'intervallo
b = 4.5;
x = a:0.001:b; % punti del grafico
k=input('k = ') % numero di nodi
%
% Interpolazione nei nodi come zeri dei pol. Cheb. 
for i=1:k
    t(i)=cos((2*(i-1)+1)*pi/2/k); % t in [-1,1]
end
X = (b-a)*t/2+(b+a)/2; % conversione da t ad X
Y = 1./(1+power(X,2)); % funzione da interpolare
PL = zeros(1, length (x)); 
for i=1:length(x),
  PL(i) = IntLag(X,Y,x(i));
end
%
% Interpolazione in nodi ugualmente spaziati
X1 = linspace(a, b, k); 
Y1 = 1./(1+power(X1,2)); % funzione da interpolare
PL1 = zeros(1, length (x)); 
for i=1:length(x),
  PL1(i) = IntLag(X1,Y1,x(i));
end
%
% grafico
xgraf = x; 
ygraf = 1./(1+power(xgraf,2));
%plot(xgraf,ygraf,'b-',x,PL,'r-',x,PL1,'k-',X,Y,'go');
%legend('f. Runge','Interp.Cheb','Interp.nodi ug.sp','intersezioni')
plot(xgraf,ygraf,'k-',x,PL,'r-',x,PL1,'g-');
legend('f. Runge','Interp.Cheb','Interp.nodi ug.sp')
title('funz. Runge')
er=abs(PL-ygraf);
er1=abs(PL1-ygraf);
normCheb=norm(er,inf)
normUgSp=norm(er1,inf)
clear;