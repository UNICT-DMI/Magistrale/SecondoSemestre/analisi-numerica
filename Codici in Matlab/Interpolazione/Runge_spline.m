% Funzione di Runge e spline cubica
%
xi = linspace(-1,1,12);
y = 1./(1+25*xi.^2);

xg = linspace(-1,1);
yg = 1./(1+25*xg.^2);

pp=spline(xi,y);
[xi,a,N,m]=unmkpp(pp);
yy=ppval(pp,xg);
ad=[3*a(:,1) 2*a(:,2) a(:,3)];
ppd=mkpp(xi,ad);
yyd=ppval(ppd,xg);

yd = -50*xg./(1+25*xg.^2).^2;

plot(xi,y,'o',xg,yg,'g',xg,yy,'r:');
figure;
plot(xg,yd,'g',xg,yyd,'r:');

% title('Funzione di Runge')
% xlabel('x')
% ylabel('f')
