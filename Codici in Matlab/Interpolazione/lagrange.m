%function [L]=lagrange(n)
% Polinomi di Lagrange di grado n+1
clear all;
n=6;
x = linspace(-1,1,100);
x1 = linspace(-1,1,n+1);
%
for jj=1:100
  for i=1:n+1
    L(i,jj)=1;
    for j=1:n+1
      if j~=i
         L(i,jj)=L(i,jj)*(x(jj)-x1(j))./(x1(i)-x1(j));
      end
    end
  end
end
%
plot(x,L(1,:),x,L(2,:),x,L(3,:),x,L(4,:),x,L(5,:))
legend('L1','L2','L3','L4','L5')
