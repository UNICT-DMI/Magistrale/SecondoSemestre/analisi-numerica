% Funzione di Runge
x = linspace(-5,5,100);
f_rung = 1./(1+x.^2);
plot(x,f_rung)
title('Funzione di Runge')
xlabel('x')
ylabel('f')
