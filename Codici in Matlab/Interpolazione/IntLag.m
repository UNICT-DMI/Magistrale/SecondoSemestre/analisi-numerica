function [ PL ] = IntLag( X, Y, x )
%---------------------------------------------------------------------------
%IMPLEMENTAZIONE DELL'INTERPOLAZIONE LAGRANGIANA.
%
% INPUT
%   X   vettore delle ascisse (ovvero i nodi)
%   Y   vettore delle ordinate (ovvero i valori di f in corrispondenza dei nodi)
%   x   valore della x corrente
% OUTPUT
%   PL  vettore dei coefficienti per la polinomiale di Lagrange
%---------------------------------------------------------------------------

n = length(X);% numero di nodi
L = zeros (1,n);%per ottimizzare, diamo una dimensione iniziale a L
% calcolo i polinomi della base associati al nodo i-esimo (ossia L_i)
for i=1:n
    % poich� L_i (x_j) = d_i_j � 1 se i=j, si ha:
    L(i)=1;
    % calcolo le altre L_i
    for j=1:n % calcoliamo la produttoria dei nodi j (con j ~= i)
        if j ~= i,
            L(i) = L(i)*(x-X(j)) / (X(i)-X(j));
        end
    end
end
% Dalla sommatoria dei prodotti delle Y(i) per i polinomi della 
% base, calcoliamo il polinomio di Lagrange:
PL = sum(L.*Y);