% polinomi
n=100;
%
subplot(2,2,1);
x=linspace(-1,1,n);
x2=[-1 0 1];
y22=[1 0 1];
p2=polyfit(x2,y22,2);
y2=polyval(p2,x);
plot(x,y2,'-r')
title('polinomio di 2� grado')
xlabel('x')
ylabel('y')
%
subplot(2,2,2);
x=linspace(-1,1,n);
x3=[-1 -.5 0 1];
y33=[0 5 0 0];
p3=polyfit(x3,y33,3);
y3=polyval(p3,x);
plot(x,y3,'-r')
title('polinomio di 3� grado')
xlabel('x')
ylabel('y')
%
subplot(2,2,3);
x=linspace(-1,1,n);
f4=(x-1).*(x-0.5).*(x+0.5).*(x+1);
plot(x,f4,'-r')
title('polinomio di 4� grado')
xlabel('x')
ylabel('y')
%
subplot(2,2,4);
x=linspace(-1,1,n);
f7=(x-1).*(x-.75).*(x-.5).*(x-0.25).*(x+0.5).*(x+1).*(x+0.25).*(x+0.75);
plot(x,f7,'-r')
title('polinomio di 7� grado')
xlabel('x')
ylabel('y')





%a=axis;
%axis([a(1) 6 a(3) a(4)])