% Interpolazione della funzione di Runge 
%
clear all;
a = -4.5; % estremi dell'intervallo
b = 4.5;
n = 15; % numero di nodi
X = linspace(a, b, n); % nodi in cui interpolare la Y
Y = 1./(1+power(X,2)); % funzione da interpolare
x = a:0.1:b;
PL = zeros(1, length (x)); 
for i=1:length(x),
  PL(i) = IntLag(X,Y,x(i));
end
% grafico
xgraf = x; 
ygraf = 1./(1+power(xgraf,2));
plot(xgraf,ygraf,'b-',x,PL,'r--',X,Y,'go');
legend('f. Runge','f interpolata','intersezioni')
title('funz. Runge')
er=abs(PL-ygraf);
m=norm(er,inf)
clear;