%polCheb1.m
%Calcola e visualizza i grafici dei primi 7 polinomi 
%di Chebichev 
%valutati in punti fissati dell'intervallo [-1,1].

clear all;
x=[-1:0.02:1];
y1=1;
y2=x;
y3=2.*x.*y2-y1;
y4=2.*x.*y3-y2;
y5=2.*x.*y4-y3;
y6=2.*x.*y5-y4;
y7=2.*x.*y6-y5;
subplot(3,2,1)
plot(x,y2,x,0);title('Chebichev 1�grado');
subplot(3,2,2)
plot(x,y3,x,0);title('Chebichev 2�grado');
subplot(3,2,3)
plot(x,y4,x,0);title('Chebichev 3�grado');
subplot(3,2,4)
plot(x,y5,x,0);title('Chebichev 4�grado');
subplot(3,2,5)
plot(x,y6,x,0);title('Chebichev 5�grado');
subplot(3,2,6)
plot(x,y7,x,0);title('Chebichev 6�grado');

