% confronta i tempi di calcolo fra Horner e classico 
% per l'approssimazione di Pad� di exp(-x)
% uso della notazione puntuale

clear all
x = linspace(0,1,5);
%tic
tic
num = 120-60*x+12*x.^2-x.^3;
den = 120+60*x+12*x.^2+x.^3;
toc

%% schema di Horner
%tic
%num = 120+x.*(-60+x.*(12-x));
%den = 120+x.*(60+x.*(12+x));
%toc
%toc

y = num./den;
[x' y']