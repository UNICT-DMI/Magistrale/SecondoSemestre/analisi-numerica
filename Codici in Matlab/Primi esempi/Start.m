%Il mio primo script con grafico
clear  
clc  % pulisce lo schermo

x = linspace(0,2*pi);
y1 = sin(x);
y2 = cos(x);
%plot(x,y1,'r')
plot(x,y1,'r',x,y2,'bs')
figure(1)
plot(x,y1,'r')
figure(2)
plot(x,y2,'b')
%axis equal
% xlabel('\it{ascisse}'); ylabel('\it{ordinate}'); 
% axis([min(x) max(x) min(y1) max(y1)+0.3]) 
% title('\it{Funzioni trigonometriche}')
% legend('seno','coseno')
% text(4,0.8,'\it{grafico}')

