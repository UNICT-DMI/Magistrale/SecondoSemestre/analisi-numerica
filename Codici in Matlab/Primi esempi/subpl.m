% fa i grafici con subplot
x=linspace(-2,2);
k=1;
y=exp(-x.^2).*cos(k*pi*x);
subplot(2,2,1)
plot(x,y); title('k=1');
k=2;
y=exp(-x.^2).*cos(k*pi*x);
subplot(2,2,2)
plot(x,y); title('k=2');
k=3;
y=exp(-x.^2).*cos(k*pi*x);
subplot(2,2,3)
plot(x,y); title('k=3');
k=4;
y=exp(-x.^2).*cos(k*pi*x);
subplot(2,2,4)
plot(x,y); title('k=4');