%disegna una superficie e le sue curve di livello
n=50; m=50;
x=linspace(0,1,n);
y=linspace(0,1,m);
[X Y]=meshgrid(x,y);

Z=X.*(1-X).*Y.*(1-Y); % f(x,y)=x(1-x)y(1-y)

subplot(2,2,1);
surf(X,Y,Z);
axis('square');
xlabel('x');ylabel('y');zlabel('z');

subplot(2,2,2);
contour(X,Y,Z);
axis('square');

subplot(2,2,3);
mesh(Z);