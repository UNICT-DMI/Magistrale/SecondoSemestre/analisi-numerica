% gestione di function handle

%con l'uso di @
% f = @(x) sin(x);
% x=linspace(-pi,pi);
% fun=f(x);
% plot(x,fun,'r')

% con l'uso di inline
f=inline('sin(x)');
x=linspace(-pi,pi);
fun=f(x);
plot(x,fun,'r')