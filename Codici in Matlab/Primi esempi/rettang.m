function [A,p,d] = rettang(a,b)
%function  rettang(a,b)
% calcola l'area A, il perimetro p e la diagonale d
% di un rettangolo di lati a,b.
% [A,p,d] = rettang(a,b)
if nargin==1
   b=5;
end

A = a * b;
p = 2 * (a + b);
d = sqrt( a^2 + b^2);