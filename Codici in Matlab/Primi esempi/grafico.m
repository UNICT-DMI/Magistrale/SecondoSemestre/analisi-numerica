% grafico di cos e sin
n=50;
x=linspace(0,2*pi,n);
y1=cos(x);
y2=sin(x);

%plot(x,y1,'r--',x,y2,'g:')


% plot(x,y1,':*g')
% hold on
% plot(x,y2,'--')
%plot(x,y1,':*g',x,y2,'b')
% legend('coseno','seno')
% gtext('f=>cos(x)')

plot(x,y1,'-g',x,y2,'--r',[1 5]*pi/4,[1 -1]/sqrt(2),'o')

title('grafico')
xlabel('x')
ylabel('y')
legend('coseno','seno','intersezione')

%gtext('GRAFICO')
a=axis;
axis([a(1) 6 a(3) a(4)])