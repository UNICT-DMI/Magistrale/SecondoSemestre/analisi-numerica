function x=sost_b(U,b)
%sostituzione all'indietro
%U matrice triangolare superiore
%
N = length(b);
S = size(U);
if (S(1) ~= S(2)) || (S(1) ~= N)
    disp('Dimensioni non corrette');
    return;
end
x=zeros(N,1);
%verifica U non singolare
if prod(diag(U))==0   % n.b. ==
   disp('Matrice non invertibile');
   return;
end
%
%sostituzione backward
for j=N:-1:2
    x(j)=b(j)/U(j,j);
    b(1:j-1)=b(1:j-1)-x(j)*U(1:j-1,j);
end
x(1)=b(1)/U(1,1);

