function [x,piv]=Gauss_piv(A,b)
% piv rappresenta le eventuali permutazioni 
%
[n,m] = size(A);
piv = 1:n;
x = zeros(n,1);
for k = 1:n-1
    % ricerca pivot
    [maxa,r] = max(abs(A(k:n,k)));
    l = r+k-1;
    %scambio righe
    piv([k l]) = piv([l k]);
    indici = piv(k+1:n);
    if A(piv(k),k)~=0
        A(indici,k) = A(indici,k)/A(piv(k),k);
        A(indici,k+1:n) = A(indici,k+1:n)-A(indici,k)*A(piv(k),k+1:n);
        b(indici) = b(indici)-A(indici,k)*b(piv(k));
    else
        disp('Matrice singolare')
        x = [];
        return;
    end
end
x = sost_b(A(piv,:),b(piv))
piv




