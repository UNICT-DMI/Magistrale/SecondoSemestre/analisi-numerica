function [A] = DOT_ijk (A)
[n,n] = size (A);
for i=1:n
	for j=2:i
		A(i,j-1) = A(i,j-1)/A(j-1,j-1);
		for k=1:j-1,			
			A(i,j) = A(i,j)-A(i,k)*A(k,j);
		end
	end
	for j=i+1:n
		for k=1:i-1 
			A(i,j) = A(i,j)/A(j,j);
		end
	end
end









