% Risoluzione Ax=b 
% inversione delle righe 
% x=GaussNaif(A,b)
%
% mostra la stabilit� 
% � come risol ma ha le righe invertite
disp('-----------------------------------------------');
fprintf('\t eps\t \t \t  x(1)\t  \t \t x(2)\n');
disp('-----------------------------------------------');

eps = 1;
P = [0 1; 1 0];
b = [2 4]';
Pb= P*b;
for i = 1:9
  eps = eps / 100;
  A = [eps 1; 1 1];
  PA = P*A;
  x = GaussNaif(PA,Pb);
  fprintf('%e\t %2.10f\t %2.10f\n',[eps;x(1);x(2)]);
end





    
  

   
      
