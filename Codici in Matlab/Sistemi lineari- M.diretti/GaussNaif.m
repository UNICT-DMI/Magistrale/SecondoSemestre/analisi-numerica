function x=GaussNaif(A,b)
% Gauss-naif
% x=Gauss-naif(A,b)
% Prerequisiti: A non singolare, A(i,i) non nulli per i=1:n.    
% La funzione restituisce in x la soluzione del sistema Ax=b 

n = length(b);
x = zeros(n,1);

for k = 1:n-1 % forward elimination
  for i = k+1:n
    moltipl = -A(i,k)/A(k,k);
    for j = k+1:n
      A(i,j) = A(i,j) + moltipl*A(k,j);
    end
    b(i) = b(i) + moltipl*b(k);
  end
end

% back substitution
x(n) = b(n)/A(n,n);
for i = n-1:-1:1
  sum = b(i);
  for j = i+1:n
    sum = sum - A(i,j)*x(j);
  end
  x(i) = sum/A(i,i);
end

return;