% Gauss-naif
% Prerequisiti: A non singolare, A(i,i) non nulli per i=1:n.
%      
% La funzione restituisce in x la soluzione del sistema Ax=b 
% 
%
A = [1 1 -2; 1 0 1; -2 1 -1]
b = [1 0 -3]'
x=GaussNaif(A,b)

