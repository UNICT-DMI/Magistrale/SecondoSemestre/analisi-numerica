function [A] = SAXPY_kji (A)
[n,n] = size (A);
for k=1:n-1
	A(k+1:n,k) = A(k+1:n,k)/A(k,k);
	for j=k+1:n,	
        for i=k+1:n
			A(i,j) = A(i,j)-A(i,k)*A(k,j);
        end
    end
end






