function [A] = GAXPY_jki (A)
[n,n] = size (A);
for j=1:n
	for k=1:j-1,	
        for i=k+1:n
			A(i,j) = A(i,j)-A(i,k)*A(k,j);
        end
    end
	for i=j+1:n, 
		A(i,j) = A(i,j)/A(j,j);
	end
end







