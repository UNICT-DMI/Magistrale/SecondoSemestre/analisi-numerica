function [x,iter]=GaussSeidel(A,b,x0,epsilon,upper)
%GAUSS_SEIDEL
% function [x,passi]=Gauss_Seidel(A,b,x0,epsilon,upper)
% Pre:  A non singolare. Perche' il problema sia ben 
%	condizionato dev'essere A a diagonale dominante
%
% La function risolve il sistema lineare Ax=b con il metodo
% di Gauss Seidel.
% Riceve in x0 il vettore di innesco, in epsilon l'accuratezza
% desiderata, in  upper un limite superiore sul numero di 
% passi da eseguire.
% Restituisce in x la soluzione del sistema e in count il numero
% di passi che sono  stati effettivamente necessari per arrivare
% alla soluzione. Se passi=upper si consiglia  di riprovare con 
%un valore di upper piu' alto.
%
n=length(b);
x=x0;

 for k=1:upper
    x_prec=x;
 
    for i=1:n
        s=b(i);
        for j =1:i-1
            s=s-A(i,j)*x(j);
        end
        for j = i+1:n
            s=s-A(i,j)*x(j);
        end
        x(i)=s/A(i,i);
        %x(i)=x(k);
    end
    %y = x'

   iter=k;
    if (norm(x-x_prec)/norm(x))<epsilon       
        return;
    end
    
end
return;


