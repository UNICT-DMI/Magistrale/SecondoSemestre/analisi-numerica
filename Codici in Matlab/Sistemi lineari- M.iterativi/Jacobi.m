function [x,iter]=Jacobi(A,b,x0,epsilon,upper)
%JACOBI
% x=Jacobi(A,b,x0,epsilon)
% Pre: A non singolare, A(i,i) non nulli per i=1:n.
%      In realta' perche'il problema sia ben condizionato, 
%      A deve 
% essere  a diagonale dominante.
% La funzione restituisce in x la soluzione del sistema Ax=b, 
% risolto con il metodo semiiterativo di Jacobi.
% Riceve in x0 il vettore di innesco e in epsilon la tolleranza 
% desiderata.
% 
n=length(b);
x=x0;
for k=1:upper
    x_old=x;
   for i=1:n
      som=0;
      for j=1:n
         if i~=j
            som=som+A(i,j)*x_old(j);
         end
      end
      som=(b(i)-som)/A(i,i);
      x(i)=som;   
   end
   iter=k;
   if (norm(x-x_old)/norm(x))<epsilon 
        return;
    end
 
end
return
   
      
